package MyApp::Example;
use Mojo::Base 'Mojolicious::Controller';
use Mango;

has 'Mango' => sub { return Mango->new(); }; 

# This action will render a template
sub welcome {
  my $self = shift;

  # Render template "example/welcome.html.ep" with message
  $self->render(msg => 'Welcome to the Mojolicious real-time web framework!');
}
#action
sub getToken
{
	my $self = shift;
	my $dataCursor = 	$self->Mango->db('AI')->collection('token')->find();
	$self ->render(json=> $dataCursor->all());

}

sub postToken
{
	my $self = shift;
	my $token = $self->param('token');
	$self->Mango->db('AI')->collection('token')->insert({"name" => $token});
	$self ->render(text => $token);

}
1;

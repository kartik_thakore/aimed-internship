package MyApp;
use Mojo::Base 'Mojolicious';

# This method will run once at server start
sub startup {
  my $self = shift;

  # Documentation browser under "/perldoc"
  $self->plugin('PODRenderer');

  # Router
  my $r = $self->routes;

  # Normal route to controller
  $r->get('/')->to('example#welcome');
  $r->get('/test')->to('example#test');

  $r->get('/token')->to('example#getToken');
  $r->post('/token')->to('example#postToken');
  $r->post('/')->to('example#insertToken');
}

1;
